var express = require('express')
var mongoose = require('mongoose')

// Middlewares
var bodyParser = require('body-parser')
var logger = require('morgan')
var errorHandler = require('errorhandler')

// Setup express and mongodb connection through mongoose
var app = express()
//var dbUri = 'mongodb://appuser:apppass@dogen.mongohq.com:10054/tsims-product'
var dbUri = 'mongodb://localhost:27017/categoryApp'
var dbConnection = mongoose.createConnection(dbUri)

var Schema = mongoose.Schema

var categorySchema = new Schema({
    _id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: String,
    path: {
        type: String,
        required: true
    },
    level: Number
})

// Virtuals
categorySchema.set('toJSON', {
    getters: true,
    virtuals: true
});
categorySchema.virtual('parent').get(function() {
    var parent,
        temp = this.path.match(/[^\,]+?(?=\,)/gi)
    if (temp.length > 1) {
        parent = temp[temp.length - 2]
    } else {
        parent = ''
    }
    return parent
})

categorySchema.pre('save', function(next) {

    if (!this.path) {
        this.path = "," + this._id + ","
        console.log("this.path" + this.path)
    }

    var numComma = occurrences(this.path, ",") - 1
    this.level = numComma

    next()
})

// Create Mongoose Model
var Category = dbConnection.model('Category', categorySchema, 'categories')

// Connect to Client
app.use(express.static('client'))

// Use middlewares in all routes
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

// Home Route
app.get('/api', function(req, res) {
    res.send('Application is Running...')
})

// Category Route
app.get('/api/categories', function(req, res, next) {
    Category.find({}, {}, {
        sort: {
            level: 1,
            name: 1
        }
    }, function(error, categories) {
        if (error) return next(error)
        res.send(categories)
    })
})
app.get('/api/categories/:id', function(req, res, next) {
    Category.findOne({
        _id: req.params.id
    }, function(error, category) {
        if (error) return next(error)
        res.send(category.toJSON())
    })
})
app.post('/api/categories', function(req, res, next) {
    var category = new Category(req.body)

    category.save(function(error, results) {
        if (error) return next(error)
        res.send(results)
    })
})
app.put('/api/categories/:id', function(req, res, next) {

    Category.findOne({
        _id: req.params.id
    }, function(error, category) {
        if (error) return next(error)
        category.set(req.body)
        category.save(function(error, results) {
            if (error) return next(error)
            res.send(results.toJSON())
        })
    })
})
app.delete('/api/categories/:id', function(req, res, next) {
    Category.remove({
        $or: [{
            _id: req.params.id
        }, {
            path: {
                $regex: ".*" + req.params.id.valueOf() + ".*"
            }
        }]
    }, function(error, results) {
        if (error) return next(error)
        res.send(results)
    })
})

// Use error handler middleware
app.use(errorHandler())

var server = require('http').createServer(app).listen(4000)

/* Function count the occurrences of substring in a string;
 * @param {String} string   Required. The string;
 * @param {String} subString    Required. The string to search for;
 * @param {Boolean} allowOverlapping    Optional. Default: false;
 * SOURCE: 
 */
function occurrences(string, subString, allowOverlapping) {

    string += "";
    subString += "";
    if (subString.length <= 0) return string.length + 1;

    var n = 0,
        pos = 0;
    var step = (allowOverlapping) ? (1) : (subString.length);

    while (true) {
        pos = string.indexOf(subString, pos);
        if (pos >= 0) {
            n++;
            pos += step;
        } else break;
    }
    return (n);
}
