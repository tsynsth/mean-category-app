var app = angular.module('app', ['ngResource', 'ngRoute']);
/* CONFIG */
app.config(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'category.html',
        controller: 'CategoryCtrl as catCtrl'
    });
});
/* FACTORY */
app.factory('Category', function($resource) {
    return $resource('/api/categories/:_id', {}, {
        'update': {
            method: 'PUT'
        }
    });
});
/* CONTROLLER */
app.controller('CategoryCtrl', function($scope, $filter, $timeout, $route, Category, pathToTree, errHandling) {

    var errIntvl = 2000;

    this.categories = Category.query(function(res) {
        $scope.categoriesTree = pathToTree.convert(res);
    }, function(error) {
        $scope.categoriesTree = "No Data: " + error;
    });

    this.parentToPath = function(objPrt, idCld) {
        if (objPrt.path === '') {
            this.selDoc.path = ",";
        } else {
            if(objPrt.path.indexOf(idCld)) objPrt.path = objPrt.path.replace(',' + idCld, '');
            this.selDoc.path = objPrt.path + idCld + ",";
        }
    };
    this.create = function(state) {
        if (state === 'set') {
            this.selDoc = new Category({
                _id: "undefined",
                name: "",
                path: ",undefined,",
                parent: ","
            });
            this.catsNoSelDoc = this.categories;
        } else {
            if (!this.selDoc._id || this.selDoc._id === "undefined") {
                this.selDoc._id = stringToSlug(this.selDoc.name);
            }
            if (this.selDoc.path.indexOf("undefined")) this.selDoc.path = this.selDoc.path.replace("undefined", this.selDoc._id);
            this.selDoc.$save(function() {
                $route.reload();
            }, function(errorRes) {
                console.log(errorRes)
                $scope.error = errHandling(errorRes);
                $timeout(function() {
                    $scope.error = '';
                }, errIntvl);
            });
        }
    };

    this.getSelected = function(id) {

        console.log("Selected ID:", id)

        var matchedObj = $filter('filter')(this.categories, {
            _id: id
        }, true);
        if (matchedObj.length) {
            this.selDoc = matchedObj[0];
            this.selDoc.objParent = $filter('filter')(this.categories, {
                _id: this.selDoc.parent
            }, true)[0];
            this.catsNoSelDoc = $filter('filter')(this.categories, {
                _id: "!" + id
            }, true);
        } else {
            console.log('Not found')
            this.selDoc = 'Not found';
        }
        this.selDocOld = this.selDoc._id;
    }
    this.updSelected = function(id) {

        var deleteOld = false;
        console.log(this.selDoc._id, this.selDoc.name)
        if(!this.selDoc._id && !this.selDoc.name) {
            $scope.error = [];
            $scope.error.push('Both ID and Name are required.');
            $timeout(function() {
                $scope.error = '';
            }, errIntvl); // this should be in service
            return;
        }
        if (!this.selDoc._id || this.selDocOld !== this.selDoc._id) { // When id changed needs to delete old and create a new 
            id = this.selDoc._id = stringToSlug(this.selDoc.name);
            deleteOld = true;
            this.create('create');
        }
        this.parentToPath(this.selDoc.objParent, id); // should make parentToPath as a service; used twice

        Category.update({
            _id: id
        }, this.selDoc, function() {

        }, function(errorRes) {
            console.log(errorRes)
            $scope.error = errHandling(errorRes);
            $timeout(function() {
                $scope.error = '';
            }, errIntvl);
        });

        if (deleteOld) {
            Category.delete({
                _id: this.selDocOld
            });
            $route.reload();
        }
    };
    this.delSelected = function(id) {
        Category.delete({
            _id: id
        }, function() {
            $route.reload();
        });
    };
    this.clean = function() {
        this.selDoc = {};
    };
    this.path = '';
    this.setPath = function(path) {
        this.path = path;
    };
});
/* FILTER */
app.filter('capitalize', function() {
    return function(input, param) {
        if (input) {
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        } else {
            return '';
        }
    }
});
// app.filter('shouldShow', function() {
//     return function(arr) {
//         return arr.map(function(obj) {
//             console.log(obj._id, obj._id !== 'book')
//             return obj._id !== 'book';
//         });
//     };
// });
app.factory('errHandling', function() {

    return function(errorRes) {
        var output = [];
        if (errorRes.error && Res.data.error && typeof errorRes.data.error.errors !== "undefined") {
            output.push(errorRes.data.error.errors.name.message);
        } else {
            output.push(errorRes.statusText)
            output.push(errorRes.data)
        }
        return output;
        $route.reload();
    }

});
/* SERVICE
 * 'pathToTree': 
 * Convert model from matrialized path to embedded document format (tree structure)
 */
app.service('removeSelDoc', function() {
    this.parents = $filter('filter')(this.categories, {
        _id: this.selDoc
    });
});
app.service('pathToTree', function() {

    this.convert = function(obj) {
        var output = [];
        /*****************
         *
         * NOTE: 
         * "obj" needs to be sorted by the key "level" preliminarily to make this service work;
         * Currently this is done in Api side with Mongoose sort();
         * "newNode" should be created from top to bottom.
         *
         *****************/
        for (var i = 0; i < obj.length; i++) { // Loop across docs
            var path = obj[i].path.match(/[^\,]+?(?=\,)/gi),
                name = obj[i].name;

            var currentNode = output;

            for (var j = 0; j < path.length; j++) { // Loop across path
                var targetedNode = path[j];
                var targetedNodeName = name;

                var lastNode = currentNode;
                for (var k = 0; k < currentNode.length; k++) { // Loop across path
                    if (currentNode[k]._id == targetedNode) {
                        currentNode = currentNode[k].children;
                        break;
                    }
                }
                // Create a new child node if not found in db
                if (lastNode == currentNode) {
                    var newNode = currentNode[k] = {
                        _id: targetedNode,
                        name: targetedNodeName,
                        children: []
                    };
                    currentNode = newNode.children;
                }
            }
        }
        return output;
    }
    this.pretty = function(obj) {
        console.log(JSON.stringify(this.convert(obj), null, 2));
    }
});
/* DIRECTIVE
 * 'tree': 
 * Display embeded documents in HTML list format
 */
app.directive('tree', function($compile) {
    return {
        restrict: 'E',
        terminal: true,
        require: "^ngController",
        scope: {
            doc: '=',
            parentData: '=',
            selDoc: '='
        },
        link: function(scope, element, attrs, parentCtrl) {
            var template = '<strong>{{doc.name}}</strong><button ng-click-conf="{msg: \'The selected category {{doc.name}} and all its children are about to be removed. Click OK to confirm the deletion. \', method: \'delSelectedFunc(doc._id)\'}" ng-show="doc.name" title="Delete" style="float:right">Delete</button><button ng-click="getSelectedFunc(doc._id); setPathFunc(\'update\'); setPathFunc(\'update\');" ng-show="doc.name"  style="float:right">Update</button>';
            if (angular.isArray(scope.doc.children)) {
                template += '<ul class="indent"><li ng-repeat="child in doc.children"><tree doc="child" parent-data="doc.children"></tree></li></ul>';
            }
            scope.setPathFunc = function(path) {
                parentCtrl.setPath(path)
            }
            scope.getSelectedFunc = function(id) {
                parentCtrl.getSelected(id)
            }
            scope.delSelectedFunc = function(id) {
                parentCtrl.delSelected(id)
            };

            var newElement = angular.element(template);
            $compile(newElement)(scope);
            element.replaceWith(newElement);
        }
    }
});
/* DIRECTIVE
 * 'ngClickConf': 
 * Confirmation prompt before a critical execution like data removal
 */
app.directive('ngClickConf', [
    function() {
        return {
            link: function(scope, element, attr) {
                var attr = eval('(' + attr.ngClickConf + ')');
                var msg = attr.msg || "Are you sure?";
                var postClick = attr.method;
                element.bind('click', function(event) {
                    if (window.confirm(msg)) {
                        scope.$eval(postClick)
                    }
                });
            }
        };
    }
]);

function stringToSlug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaeeeeiiiioooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}
