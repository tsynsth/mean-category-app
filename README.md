# README #

### Summary ###

A simple MEAN CRUD Category application.

### Prerequisites ###

You need to have those before installation:

* Node.js
* MongoDB
* Internet Connection

### Installation ###

1: Clone or download this repository.

2: Install the Node.js dependencies:

```
#!javascript

$ npm install
```

3: Run the app:

```
#!javascript

$ node index.js
```

### Dependencies ###

* [Mongoose](http://mongoosejs.com/)
* [Express 4.x](http://expressjs.com/)
* [Express bodyParser](https://github.com/expressjs/body-parser)
* [Express Morgan](https://github.com/expressjs/morgan)


* [AngularJS](https://angularjs.org/)
* AngularJS ngResource
* AngularJS ngRoute
* [Bootstrap](http://getbootstrap.com/)

### Database ###

This application uses the next MongoDB configuration. You may change any of those in `index.js`.

* Database name: categoryApp
* Collection name: categories

To bulk-import sample data from "sampleData.json" to local MongoDB installation,
in CLI, change directory to the app folder and run:

```
mongoimport -d categoryApp -c categories --jsonArray < sampleData.json
```

### Reference ###

CRUD Reference
http://www.sitepoint.com/creating-crud-app-minutes-angulars-resource/

JSON formmater
http://jsonformatter.curiousconcept.com/

**Angular**

$resource
https://docs.angularjs.org/api/ngResource/service/$resource

Service
http://viralpatel.net/blogs/angularjs-service-factory-tutorial/

Conversion from Material Path to Tree

http://stackoverflow.com/questions/6232753/convert-delimited-string-into-hierarchical-json-with-jquery
http://docs.mongodb.org/manual/tutorial/model-tree-structures-with-materialized-paths/
http://stackoverflow.com/questions/21366735/objects-from-materialized-path

this vs $scope

http://stackoverflow.com/questions/11605917/this-vs-scope-in-angularjs-controllers
http://toddmotto.com/digging-into-angulars-controller-as-syntax/
http://stackoverflow.com/questions/16389362/angularjs-service-query-returning-zero-result#

$filter Capitalize 
http://angular-tips.com/blog/2013/08/why-does-angular-dot-js-rock/

$timeout
http://stackoverflow.com/questions/16824157/angularjs-directive-how-do-i-hide-the-alert-using-timeout

Notification with Angular
http://derekries.github.io/angular-notifications/

**MongoDB**

mongoimport
http://docs.mongodb.org/manual/reference/program/mongoimport/

Import Data File
http://stackoverflow.com/questions/20416840/where-to-load-bulk-data-in-the-meanmongoose-stack

#### JavaScript General ####

Occurence of Substring on String

http://stackoverflow.com/questions/4009756/how-to-count-string-occurrence-in-string


### Research Codes on Plunker ###

Module codes researched that were/weren't used in this app.

* [Angular Select Field with ng-options and ng-model](http://embed.plnkr.co/uXsZWX/preview)

* [Angular Filter Order Object by a Field](http://embed.plnkr.co/Br6cXeUnJqXZKNSuyglq/preview) (Source: http://stackoverflow.com/questions/14478106/angularjs-sorting-by-property)

* [Angular Filter Get Object by ID](http://embed.plnkr.co/7fzFObR1n1jYgqzTN1cr/preview) (Source: http://stackoverflow.com/questions/15610501/in-angular-i-need-to-search-objects-in-an-array)


